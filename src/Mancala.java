import java.util.Random;


public class Mancala extends Game{
	private final int PEBBLE_COUNT = 4;
	
	public int iPlayer1 = 0;
	public int iPlayer2 = 0;
	public int[][] board;
	
	public int nextTurn = 0;
	
	private GameView gv;
	
	Agent a, b;
	
	public Mancala(Agent a, Agent b) {
		super(a, b);
		this.a = a;
		this.b = b;
		board = new int[2][6];
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 6; j++)
				board[i][j] = PEBBLE_COUNT;
		}
		System.out.println("Called");
		gv = new GameView(board, a, b);
	}

	@Override
	boolean isFinished() {
		boolean flag = false;
		
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 6; j++){
				if(board[i][j] != 0){
					flag = true;
					break;
				}
			}
			if(!flag)
				return true;
		}
				
		return false;
	}

	@Override
	void initialize(boolean fromFile) {
		// TODO Auto-generated method stub
		
	}

	@Override
	void showGameState() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void updateMessage(String msg) {
		// TODO Auto-generated method stub
		gv.Refresh();
	}
	
	@Override
	public void play(){
		nextTurn = new Random().nextInt(2);
		
		while(!isFinished()){
			System.out.println(Integer.toString(nextTurn));
			gv.turn = nextTurn;
			gv.Refresh();
			agent[nextTurn].makeMove(this);
			showGameState();
		}
	}
	
	public int checkForWin(){
		if(isFinished()){
			return (iPlayer1 > iPlayer2)?0:1;
		}
		return -1;
	}
	
	
	
	public void generateBoard(int col, int playerNum, boolean print) {
		int total = board[playerNum][col];
		
		board[playerNum][col] = 0;
		int myRow = playerNum;
		nextTurn = 1 - playerNum;
		//System.out.println("GENERATING BOARD");
		while(total != 0){
			//System.out.println(col + " " + total);
			col = col - (1 - myRow) + myRow;
			if(col < 0){
				myRow = 1;
				col = 0;
				
				if(playerNum == 0){
					iPlayer1++;
					total--;
				}
				
				if(total == 0){
					nextTurn = playerNum;
					break;
				}
			}
			else if(col > 5){
				myRow = 0;
				col = 5;
				
				if(playerNum == 1){
					iPlayer2++;
					total--;
				}
				
				if(total == 0){
					nextTurn = playerNum;
					break;
				}
			}
			
			board[myRow][col]++;
			total--;
			
			if(total == 0 && board[myRow][col] > 1){
				total = board[myRow][col];
				board[myRow][col] = 0;
			}
			else if(total == 0 && board[myRow][col] == 1 && myRow == 0){
				if(playerNum == 0)
					iPlayer1 += board[1][col];
				else
					iPlayer2 += board[0][col];
				board[1-myRow][col] = 0;
			}
			if(print){
				System.out.println("COL: " + col + " TOTAL: " + total + " iPlayer1: " + iPlayer1 + " iPlayer2: " + iPlayer2);
				gv.SetPlayerScores(iPlayer1,iPlayer2);
				gv.SetRowColTotal(myRow, col, total);
				
				gv.Refresh();
				try{
					Thread.sleep(700);
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
			
		}
	}

}
