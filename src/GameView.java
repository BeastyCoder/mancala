import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GameView extends JPanel{
	private final int WINDOW_WIDTH = 1000;
	private final int WINDOW_HEIGHT = 600;
	private final int SQUARE_DIMENSION = 100;
	
	public int[][] board;
	public int iPlayer1;
	public int iPlayer2;
	public String sPlayer1;
	public String sPlayer2;
	
	public int row = 0;
	public int col = 0;
	public int total = 0;
	
	public int turn = 0;
	
	private JButton[] myButtons;
	
	private JFrame jf;
	
	public GameView(int[][] board, Agent a, Agent b){
		jf = new JFrame();
		jf.setTitle("Mancala");
		jf.setBounds(0,0,WINDOW_WIDTH,WINDOW_HEIGHT);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setContentPane(this);
		jf.getContentPane().setBackground(new Color(76,107,57,255));
		this.board = board;
		iPlayer1 = 0;
		iPlayer2 = 0;
		sPlayer1 = a.name;
		sPlayer2 = b.name;
		myButtons = new JButton[6];
		
		for(int i = 0; i < 6; i++){
			
		}
	}
	
	public void SetPlayerScores(int x1, int x2){
		iPlayer1 = x1;
		iPlayer2 = x2;
	}
	
	public void SetRowColTotal(int row, int col, int total){
		this.row = row;
		this.col = col;
		this.total = total;
	}
	
	public void Refresh(){
		System.out.println("GV REFRESH");
		this.revalidate();
		this.repaint();
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Init(g);
	}
	
	public void Init(Graphics g){
		
		g.setColor(new Color(0,0,0,80));
		g.fillRoundRect(WINDOW_WIDTH/2 - SQUARE_DIMENSION*3 + 7, WINDOW_HEIGHT/2 - SQUARE_DIMENSION + 7, 
				SQUARE_DIMENSION*6, SQUARE_DIMENSION*2, SQUARE_DIMENSION - 10, SQUARE_DIMENSION - 10);
		
		g.setColor(new Color(0,0,0,140));
		g.fillRoundRect(WINDOW_WIDTH/2 - SQUARE_DIMENSION*3 + 5, WINDOW_HEIGHT/2 - SQUARE_DIMENSION + 5, 
				SQUARE_DIMENSION*6, SQUARE_DIMENSION*2, SQUARE_DIMENSION - 10, SQUARE_DIMENSION - 10);
		
		g.setColor(new Color(0,0,0,220));
		g.fillRoundRect(WINDOW_WIDTH/2 - SQUARE_DIMENSION*3 + 3, WINDOW_HEIGHT/2 - SQUARE_DIMENSION + 3, 
				SQUARE_DIMENSION*6, SQUARE_DIMENSION*2, SQUARE_DIMENSION - 10, SQUARE_DIMENSION - 10);
		
		
		
		
		
		g.setColor(new Color(114,147,89,255));
		
		g.fillRoundRect(WINDOW_WIDTH/2 - SQUARE_DIMENSION*3 , WINDOW_HEIGHT/2 - SQUARE_DIMENSION, 
				SQUARE_DIMENSION*6, SQUARE_DIMENSION*2, SQUARE_DIMENSION - 10, SQUARE_DIMENSION - 10);
		
		
		
		for(int i = 0; i < 2; i++){
			for(int j = 0; j < 6; j++){
				//Setting the board and holes
				g.setColor(new Color(51,66,40,255));
				
				if(row == i && col == j)
					g.setColor(new Color(123,159,171,255));
				
				g.fillOval((WINDOW_WIDTH/2 - SQUARE_DIMENSION*3) + 5 + SQUARE_DIMENSION*j, 
						(WINDOW_HEIGHT/2 - SQUARE_DIMENSION) + 5 + SQUARE_DIMENSION*i, 
						SQUARE_DIMENSION - 10, SQUARE_DIMENSION-10);
				g.setColor(new Color(201,225,183,255));
				g.fillOval((WINDOW_WIDTH/2 - SQUARE_DIMENSION*3) + 10 + SQUARE_DIMENSION*j, 
						(WINDOW_HEIGHT/2 - SQUARE_DIMENSION) + 10 + SQUARE_DIMENSION*i, 
						SQUARE_DIMENSION - 20, SQUARE_DIMENSION-20);
				
				//Setting the text
				g.setColor(Color.BLACK);
				g.setFont(new Font("Consolas", Font.PLAIN, 48));
				g.drawString(Integer.toString(board[i][j]),
						(WINDOW_WIDTH/2 - SQUARE_DIMENSION*3) + 30 + SQUARE_DIMENSION*j,
						(WINDOW_HEIGHT/2 - SQUARE_DIMENSION) - 35 + SQUARE_DIMENSION*(i+1));
				
				g.setFont(new Font("Consolas", Font.PLAIN, 26));
				
				g.drawString(Integer.toString(total),
							(WINDOW_WIDTH/2 - SQUARE_DIMENSION*3) + 10 + SQUARE_DIMENSION*col,
							(WINDOW_HEIGHT/2) + 5);
				
				//My Stash
				g.setColor(turn == 0?new Color(212,65,37,255) : new Color(51,66,40,255));
				g.fillRoundRect((WINDOW_WIDTH/2 - SQUARE_DIMENSION*4) - 10,
						(WINDOW_HEIGHT/2 - SQUARE_DIMENSION*2), SQUARE_DIMENSION,
						 SQUARE_DIMENSION*2, SQUARE_DIMENSION - 20, SQUARE_DIMENSION - 20);
				
				g.setColor(turn == 1?new Color(212,65,37,255) : new Color(51,66,40,255));
				g.fillRoundRect((WINDOW_WIDTH/2 + SQUARE_DIMENSION*3) + 10,
						WINDOW_HEIGHT/2, SQUARE_DIMENSION,
						 SQUARE_DIMENSION*2, SQUARE_DIMENSION - 20, SQUARE_DIMENSION - 20);
				
				g.setColor(new Color(201,225,183,255));
				g.fillRoundRect((WINDOW_WIDTH/2 - SQUARE_DIMENSION*4) - 5,
						(WINDOW_HEIGHT/2 - SQUARE_DIMENSION*2) + 5, SQUARE_DIMENSION - 10,
						 SQUARE_DIMENSION*2 - 10, SQUARE_DIMENSION - 20, SQUARE_DIMENSION - 20);
				
				g.fillRoundRect((WINDOW_WIDTH/2 + SQUARE_DIMENSION*3) + 15,
						WINDOW_HEIGHT/2 + 5, SQUARE_DIMENSION - 10,
						 SQUARE_DIMENSION*2 - 10, SQUARE_DIMENSION - 20, SQUARE_DIMENSION - 20);
				
				//Player scores
				g.setColor(Color.BLACK);
				g.setFont(new Font("Consolas", Font.PLAIN, 52));
				g.drawString(Integer.toString(iPlayer1),
						(WINDOW_WIDTH/2 - SQUARE_DIMENSION*4) + 5,
						(WINDOW_HEIGHT/2 - SQUARE_DIMENSION) - (SQUARE_DIMENSION/4) - 5);
				
				g.setColor(Color.BLACK);
				g.setFont(new Font("Consolas", Font.PLAIN, 52));
				g.drawString(Integer.toString(iPlayer2),
						(WINDOW_WIDTH/2 + SQUARE_DIMENSION*3) + 25,
						(WINDOW_HEIGHT/2 + SQUARE_DIMENSION) + (SQUARE_DIMENSION/4) + SQUARE_DIMENSION/2);
				
				
			}
		}
	}
}
