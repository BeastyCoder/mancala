import java.util.Scanner;


public class Human extends Agent{
	
	static Scanner input = new Scanner(System.in);
	Mancala myGame;
	
	public void generateBoard(Mancala game, int col){
		int total = game.board[1][col];
		game.board[1][col] = 0;
		int myRow = 1;
		game.nextTurn = 0;
		while(total != 0){
			col = col - (1 - myRow) + myRow;
			
			if(col < 0){
				myRow = 1;
				col = 0;
			}
			else if(col > 5){
				myRow = 0;
				col = 5;
				game.iPlayer2++;
				total--;
				if(total == 0){
					game.nextTurn = 1;
				}
			}
			
			game.board[myRow][col]++;
			total--;
			
			if(total == 0 && game.board[myRow][col] > 1){
				total = game.board[myRow][col];
				game.board[myRow][col] = 0;
			}
			else if(total == 0 && game.board[myRow][col] == 1 && myRow == 1){
				game.iPlayer2 += game.board[0][col];
				game.board[0][col] = 0;
			}
		}
	}
	
	public Human(String name) {
		super(name);
		super.setRole(1);
	}

	@Override
	public void makeMove(Game game) {
		myGame = (Mancala)game;
		int dummy = input.nextInt();
		
		myGame.generateBoard(dummy, 1, true);
	}
	

}
