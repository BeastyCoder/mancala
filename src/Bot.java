
public class Bot extends Agent{
	private final int DEPTH = 5;
	private boolean flag = false;
	
	public Mancala myGame;
	int initPlayer1Score;
	int initPlayer2Score;
	public Bot(String name) {
		super(name);
		super.setRole(0);
	}
	
	@Override
	public void makeMove(Game game) {
		// TODO Auto-generated method stub
		try{
			Thread.sleep(1000);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		myGame = (Mancala)game;
		initPlayer1Score = myGame.iPlayer1;
		initPlayer2Score = myGame.iPlayer2;
		
		CellValueTuple cvt = MAX_VALUE(myGame,DEPTH,-900,+900);
		
		if(cvt.col != -1){
			System.out.println("TERMINATING " + cvt.col);
			myGame.iPlayer1 = initPlayer1Score;
			myGame.iPlayer2 = initPlayer2Score;
			myGame.generateBoard(cvt.col,0,true);
		}
		
	}
	
	//Evaluation Function
	private int EvaluationFunction(Mancala game){
		int score = game.iPlayer1;
		float temp = 0, impact = 0.6f;
		
		for(int i = 0; i < 6; i++){
			temp += game.board[0][i]*(impact--);
		}
		
		temp += score*3.4;
		score = Math.round(temp);
		
		return score;
	}
	
	//MAX 
	CellValueTuple MAX_VALUE(Mancala game, int depth, int alpha, int beta){
		CellValueTuple maxCVT = new CellValueTuple();
		maxCVT.utility = -900;
		int[][] backBoard;
		int backPlayer;
		int winner = game.checkForWin();
		
		if(winner != -1){
			maxCVT.utility = (winner == 0)?900:-900;
			return maxCVT;
		}
		else if(depth == 0){
			maxCVT.utility = EvaluationFunction(game);
			return maxCVT;
		}
		for(int i = 0; i < 6; i++){
			backBoard = new int[2][6];
			backPlayer = game.iPlayer1;
			for(int t = 0; t < 2; t++){
				for(int j = 0; j < 6; j++){
					backBoard[t][j] = game.board[t][j];
				}
			}
			//System.out.println("DEPTH: " + depth);
			game.generateBoard(i,0,false);
			int v;
			if(game.nextTurn == 1)
				v = MIN_VALUE(game,depth-1,alpha,beta).utility;
			else
				v = MAX_VALUE(game,depth-1,alpha,beta).utility;
			
			if(maxCVT.utility >= beta){
				maxCVT.col = i;
				return maxCVT;
			}
			
			if(maxCVT.utility < v){
				maxCVT.utility = v;
				maxCVT.col = i;
			}
			alpha = Math.max(alpha,maxCVT.utility);
			//System.out.println("RESETTING:");
			for(int k = 0; k < 2; k++){
				for(int j = 0; j < 6; j++){
					game.board[k][j] = backBoard[k][j];
					//System.out.print(game.board[k][j] + " ");
				}
				//System.out.println("");
			}
			//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>");
			
			game.iPlayer1 = backPlayer;
		}
		
		return maxCVT;
	}
	
	//MIN
	CellValueTuple MIN_VALUE(Mancala game, int depth, int alpha, int beta){
		CellValueTuple minCVT = new CellValueTuple();
		minCVT.utility = +900;
		int[][] backBoard;
		int backPlayer;
		int winner = game.checkForWin();
		
		if(winner != -1){
			minCVT.utility = (winner == 0)?900:-900;
			return minCVT;
		}
		else if(depth == 0){
			minCVT.utility = EvaluationFunction(game);
			return minCVT;
		}
		
		for(int i = 0; i < 6; i++){
			backBoard = new int[2][6];
			System.out.println("PLYAER2: " + game.iPlayer2);
			backPlayer = game.iPlayer2;
			for(int t = 0; t < 2; t++){
				for(int j = 0; j < 6; j++){
					backBoard[t][j] = game.board[t][j];
				}
			}
			game.generateBoard(i,1,false);
			int v;
			
			if(game.nextTurn == 1)
				v = MIN_VALUE(game,depth-1,alpha,beta).utility;
			else
				v = MAX_VALUE(game,depth-1,alpha,beta).utility;
			
			if(minCVT.utility <= alpha){
				minCVT.col = i;
				return minCVT;
			}
			
			if(v < minCVT.utility){
				minCVT.utility = v;
				minCVT.col = v;
			}
			beta = Math.max(beta,minCVT.utility);
			
			for(int k = 0; k < 2; k++){
				for(int j = 0; j < 6; j++){
					game.board[k][j] = backBoard[k][j];
				}
			}
			
			game.iPlayer2 = backPlayer;
			System.out.println("PLYAER2(A):" + game.iPlayer2);
		}
		
		
		
		return minCVT;
	}
	
	//Cell value holder class
	public class CellValueTuple{
		public int col, utility;
		
		public CellValueTuple(){
			col = -1;
		}
	}

}
